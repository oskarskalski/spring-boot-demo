package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.NetworkInterface;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

class DemoApplicationTests {

//	@Test
//	void contextLoads() {
//	}

    @Test
    void test() {
        System.out.println(Instant.now().toEpochMilli());
        IDGenerator idGenerator = new IDGenerator(5);
        int i = 0;
        while (true) {
            if (idGenerator.nextId() == 0) {
                i++;
            }

            if (i == 2) {
                break;
            }
        }
        System.out.println(Instant.now().toEpochMilli());
    }

    @Test
    void test2() {
        int i = 0;
        while (i < 5) {
            long nodeId;
            try {
                StringBuilder sb = new StringBuilder();
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = networkInterfaces.nextElement();
                    byte[] mac = networkInterface.getHardwareAddress();
                    if (mac != null) {
                        for (byte macPort : mac) {
                            sb.append(String.format("%02X", macPort));
                        }
                    }
                }
                sb.append(i);
                System.out.println(sb);
                nodeId = sb.toString().hashCode();
            } catch (Exception ex) {
                System.out.println("a");
                nodeId = (new SecureRandom().nextInt());
            }
            System.out.println(nodeId);
            i++;
        }
    }

    @Test
    void test3(){
//        String podName = "a";
//        System.out.println(podName.hashCode() % 1023);
        char[] chars = new char[36];
        int index = 0;

        // Adding a-z to the array
        for (char c = 'a'; c <= 'z'; c++) {
            chars[index++] = c;
        }
        // Adding 0-9 to the array
        for (char c = '0'; c <= '9'; c++) {
            chars[index++] = c;
        }

        // Generating all combinations
        for (int i = 0; i < chars.length; i++) {
            for (int j = 0; j < chars.length; j++) {
                for (int k = 0; k < chars.length; k++) {
                    for (int l = 0; l < chars.length; l++) {
                        for (int m = 0; m < chars.length; m++) {
                            var aa = "" + chars[i] + chars[j] + chars[k] + chars[l] + chars[m];
                            System.out.println(aa.hashCode() % 1023);
                        }
                    }
                }
            }
        }
    }


    @Test
    void aa() {
        System.out.println(Instant.now().toEpochMilli());
        System.out.println("1704839110877".length());
        System.out.println("18446744073709551615".length());

        List<String> patternList = generatePatternList(10);
        for(var k: patternList){
            System.out.println(k.hashCode());
        }
    }
    private static List<String> generatePatternList(int size) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add("test-" + generateThreeDigits() + "-" + generateTwoLetters());
        }
        return list;
    }

    private static String generateThreeDigits() {
        Random rand = new Random();
        int num = 100 + rand.nextInt(900); // 100 to 999
        return String.valueOf(num);
    }

    private static String generateTwoLetters() {
        Random rand = new Random();
        return "" + (char) ('a' + rand.nextInt(26)) + (char) ('a' + rand.nextInt(26));
    }
}

class IDGenerator {
    private long sequence = 0L;
    private final long sequenceMask;

    public IDGenerator(int sequenceBits) {
        this.sequenceMask = ~(-1L << sequenceBits);
    }

    public synchronized long nextId() {
        sequence = (sequence + 1) & sequenceMask;
        return sequence;
    }
}