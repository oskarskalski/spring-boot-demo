package com.example.demo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

//@Async
@RestController
@AllArgsConstructor
@RequestMapping("/test")
public class Controller {
    private EmployeeRepository employeeRepository;
    private AaaRepository aaaRepository;
    private EntityManager entityManager;
    private Employee2Repository employee2Repository;

    @PostMapping("/test/test/{id}")
    public Employee3 create(@PathVariable("id")Long id){
        Employee3 employee3 = new Employee3();
        employee3.setEmployeeParent(id);
        return employee2Repository.save(employee3);
    }

    @GetMapping("/test/test/{id}")
    public Employee4 get(@PathVariable("id")Long id){
        return employee2Repository.findEmployee4ById(id).orElseThrow();
    }

    @PostMapping
    public Employee add(@RequestBody Employee employee) {
        Employee currentEmployee = employee;

        for (var i = 0; i < 2; i++) {
            Employee newEmployee = new Employee();

            currentEmployee.setEmployees(Set.of(newEmployee));
            newEmployee.setEmployee(currentEmployee);


            currentEmployee = newEmployee;
        }

        Aaa aaa = new Aaa();
        aaa.setFirstName("aaa");
        aaa.setParentEmployee(employee); // This should also take care of setting the 'employee_id' in 'Aaa'.
        employee.setAaas(Set.of(aaa));

        Employee savedEmployee = employeeRepository.save(employee);
        aaaRepository.save(aaa);
        return null;
    }

    @PostMapping("/al")
    @Transactional
    public List<Employee> add() {
        List<Employee> employeeList = new ArrayList<>();
        for (var i = 0; i < 10; i++) {
            Employee employee1 = new Employee();
            employee1.setEmail("aaaaaaaaa12312312");
            employeeList.add(employee1);
            entityManager.persist(employee1);
        }
        ;
//        return employeeRepository.saveAll(employeeList);
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
//        CompletableFuture.runAsync(() -> employeeRepository.deleteById(id));
        employeeRepository.deleteById(id);
    }

    @GetMapping("/{id}")
    public Employee find(@PathVariable Long id) {
//        CompletableFuture.runAsync(() -> employeeRepository.deleteById(id));
        return employeeRepository.findById(id).orElseThrow();
    }
}

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "aaa")
class Aaa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "employee_id", insertable = false, updatable = false)
    private Long employee_id;
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee parentEmployee;

}

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "aaa")
class Bbb {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "employee_id", insertable = false, updatable = false)
    private Long employee_id;
    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    @JsonIgnore
    private Employee4 parentEmployee;

}

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee")
class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 20)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "employee_parent", insertable = false, updatable = false)
    private Long employeeParent;

    @JsonManagedReference
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private Set<Employee> employees;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "employee_parent")
    private Employee employee;

    @OneToMany(mappedBy = "parentEmployee", cascade = CascadeType.ALL)
    private Set<Aaa> aaas;
}

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@Table(name = "employee")
class Employee2 {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 20)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;
}

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")

class Employee3 extends Employee2{
    @Column(name = "employee_parent")
    private Long employeeParent;
}


@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")
class Employee4 extends Employee2{
    @JsonManagedReference
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Employee4> employees;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "employee_parent")
    @JsonIgnore
    private Employee4 employee;

    @OneToMany(mappedBy = "parentEmployee", cascade = CascadeType.ALL)
    private Set<Bbb> aaas;
}


@Repository
interface Employee2Repository extends JpaRepository<Employee2, Long> {
    Optional<Employee4> findEmployee4ById(Long id);
}

@Repository
interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
@Repository
interface AaaRepository extends JpaRepository<Aaa, Long> {

}