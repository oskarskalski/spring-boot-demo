from locust import HttpUser, between, task


class WebsiteUser(HttpUser):

    @task
    def ddd(self):
        response = self.client.post("/test", json={
                                                 "firstName":"aaa",
                                                 "lastNmae":"aaaaa",
                                                 "email":"vv"
                                             }, headers={"Content-Type":"application/json"})
        id = response.json()['id']
        self.client.delete("/test/" + str(id))
